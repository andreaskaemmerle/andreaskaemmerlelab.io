# andreaskaemmerle.gitlab.io

Source of my [GitLab Pages](https://about.gitlab.com/features/pages/) website based on [Jekyll](https://jekyllrb.com).

# Installation

```bash 
$ gem install bundler jekyll
```

# Run locally

```bash 
$ bundle exec jekyll serve
$ Browse to http://localhost:4000
```

# Contributing

1. Create an [issue](https://gitlab.com/andreaskaemmerle/andreaskaemmerle.gitlab.io/issues/new) or [merge request](https://gitlab.com/andreaskaemmerle/andreaskaemmerle.gitlab.io/merge_requests/new)
2. Assign it to @andreaskaemmerle
