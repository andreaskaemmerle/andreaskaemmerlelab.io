---
layout: content-only
title: GitLab testing
permalink: /gitlab-testing/
---

This page is used for testing external GitLab features.

<!-- toc -->
<!-- <div class="sidebar hidden-print" id="sidebar">
  <ul class="nav sidenav">
    <li class="active">
      <a href="#embedded-snippets">Embedded snippets</a>
    </li>
  </ul>
</div> -->

## Embedded snippets

Related [GitLab.com issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/8088).

---

Snippet with filename:
<script src="https://gitlab.com/akaemmerle/example-project/snippets/1714803.js"></script>

Snippet without filename:
<script src="https://gitlab.com/akaemmerle/example-project/snippets/1714813.js"></script>

[#movingtogitlab](https://twitter.com/movingtogitlab) snippet:
<script src="https://gitlab.com/snippets/1719648.js"></script>

----

## GitHub Gist

Large snippet:
<script src="https://gist.github.com/andreaskaemmerle/27c8e509786d4dd5725c9cee71afb31d.js"></script>
